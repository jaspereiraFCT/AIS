/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aisclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Joaquim
 */
public class AISClient {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        try {
            List<URL> url = new ArrayList<URL>();
           /* url.add(new URL("http://localhost:1461/api/AIS/Register_User?name=Pedro&Type=Home"));
            url.add(new URL("http://localhost:1461/api/AIS/Register_User?name=Pedro&Type=Carro"));
            url.add(new URL("http://localhost:1461/api/AIS/Register_User?name=Pedro1&Type=AC"));
            url.add(new URL("http://localhost:1461/api/AIS/Register_User?name=Pedro2&Type=AC"));
            url.add(new URL("http://localhost:1461/api/AIS/Register_User?name=Miguel&Type=Carro"));
            url.add(new URL("http://localhost:1461/api/AIS/Register_User?name=Miguel1&Type=AC"));
            url.add(new URL("http://localhost:1461/api/AIS/Register_User?name=MIguel2&Type=AC"));
            url.add(new URL("http://localhost:1461/api/AIS/Register_User?name=MIguel&Type=Home"));
            url.add(new URL("http://localhost:1461/api/AIS/Entity_Association?id_master=1&id_slave=2"));
            url.add(new URL("http://localhost:1461/api/AIS/Entity_Association?id_master=1&id_slave=3"));
            url.add(new URL("http://localhost:1461/api/AIS/Entity_Association?id_master=1&id_slave=4"));
            url.add(new URL("http://localhost:1461/api/AIS/Entity_Association?id_master=8&id_slave=5"));
            url.add(new URL("http://localhost:1461/api/AIS/Entity_Association?id_master=8&id_slave=6"));
            url.add(new URL("http://localhost:1461/api/AIS/Entity_Association?id_master=8&id_slave=7"));*/
            
            Iterator<URL> iterator = url.iterator();
            
            while (iterator.hasNext()) {
                HttpURLConnection conn = (HttpURLConnection) iterator.next().openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Accept", "application/json");

                if (conn.getResponseCode() != 200) {
                    throw new RuntimeException("Failed : HTTP error code : "
                            + conn.getResponseCode());
                }

                BufferedReader br = new BufferedReader(new InputStreamReader(
                        (conn.getInputStream())));

                String output;
                System.out.println("Output from Server .... \n");
                while ((output = br.readLine()) != null) {
                    System.out.println(output);
                }

                conn.disconnect();
            }

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
    }

}
