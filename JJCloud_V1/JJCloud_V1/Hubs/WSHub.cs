﻿using JJCloud_V1.Models;
using JJCloud_V1.Models.Repositorys;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System.Collections.Generic;

namespace JJCloud_V1.Hubs
{
    [HubName("WsHub")]
    public class WSHub : Hub
    {
        private readonly ConsultasRepository rep = new ConsultasRepository();
        private readonly GadgetRepository repG = new GadgetRepository();
        private readonly HomeTempRepository repH = new HomeTempRepository();
        private readonly LightStatusRepository repL = new LightStatusRepository();

        public IEnumerable<Consultas> GetAllConsultas()
        {
            return rep.GetAll();
        }

        public IEnumerable<GraphicPies> GetWsNamePie()
        {
            return rep.Count(false);
        }

        public IEnumerable<GraphicPies> GetWsTypePie()
        {
            return rep.Count(true);
        }

        public IEnumerable<GagdetView> GetAllGadgets()
        {
            return repG.GetAllView();
        }

        public IEnumerable<GadgetSlaveView> GetAllSlaves(long MasterID)
        {
            return repG.GetAllSlaves(MasterID);
        }

        public IEnumerable<GraphicPies> GetAllMasterPies()
        {
            return repG.Count(true);
        }
        public IEnumerable<GraphicPies> GetAllSlavePies(long masterId)
        {
            return repG.Count(false,masterId);
        }

        public HomeTemp GetHomeTemp(long id)
        {
            return repH.Get(id);
        }
        public LightStatus GetLigthStatus(long id)
        {
            return repL.Get(id);
        }

    }
}