﻿using System.Collections;
using System.Collections.Generic;

namespace JJCloud_V1.Models.Interfaces
{
    interface IConsultasRepository
    {
        IEnumerable<Consultas> GetAll();
        IEnumerable<Consultas> GetLast(int Last);
        Consultas Get(long id);
        bool Add(Consultas item);
        bool Remove(long id);
        bool Update(Consultas item);
    }
}
