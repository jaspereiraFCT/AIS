﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JJCloud_V1.Models.Interfaces
{
    interface IGadgetTypeRepository
    {
        IEnumerable<GadgetType> GetAll();
        GadgetType Get(String TypeName);
        GadgetType Add(GadgetType item);
        bool Remove(long id);
        bool Update(GadgetType item);
    }
}
