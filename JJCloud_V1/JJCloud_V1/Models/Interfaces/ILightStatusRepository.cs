﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JJCloud_V1.Models.Interfaces
{
    interface ILightStatusRepository
    {
        LightStatus Get(long id);
        LightStatus Add(LightStatus item);
        bool Update(LightStatus item);
    }
}
