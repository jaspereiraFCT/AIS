﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JJCloud_V1.Models.Interfaces
{
    interface IGadgetRepository
    {
        IEnumerable<Gadget> GetAll();
        IEnumerable<GagdetView> GetAllView();
        IEnumerable<Gadget> GetAllFrom(long houseID);
        IEnumerable<Gadget> GetAllFrom(long houseID, long type);
        Gadget add(string name, GadgetType gT);
        Gadget Get(long id);
    }
}
