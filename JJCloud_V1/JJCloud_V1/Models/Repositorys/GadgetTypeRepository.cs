﻿using JJCloud_V1.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JJCloud_V1.Models.Repositorys
{
    public class GadgetTypeRepository : BigRepository, IGadgetTypeRepository
    {
        public IEnumerable<GadgetType> GetAll()
        {
            return db.GadgetTypeSet;
        }
        public GadgetType Get(String TypeName)
        {
            return db.GadgetTypeSet.Where(b => b.typeName.ToLower() == TypeName.ToLower()).FirstOrDefault();
        }
        public GadgetType Add(GadgetType item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            GadgetType res = db.GadgetTypeSet.Add(item);
            db.SaveChanges();

            return res;
        }
        public bool Remove(long id)
        {
            db.GadgetTypeSet.Remove(db.GadgetTypeSet.Find(id));
            return db.SaveChanges() == 1 ? true : false;
        }

        public bool Update(GadgetType item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            GadgetType res = db.GadgetTypeSet.Find(item.Id);
            res.typeName = item.typeName;

            return db.SaveChanges() == 1 ? true : false;
        }
    }
}