﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JJCloud_V1.Models.Interfaces;

namespace JJCloud_V1.Models.Repositorys
{
    public class LightStatusRepository : BigRepository, ILightStatusRepository
    {
        public LightStatus Get(long id)
        {
            return db.LightStatusSet.Find(id);
        }

        public LightStatus Add(LightStatus item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            LightStatus res = db.LightStatusSet.Add(item);
            db.SaveChanges();

            return res;
        }

        public bool Update(LightStatus item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            LightStatus res = db.LightStatusSet.Find(item.Id);
            res.Status = item.Status;

            return db.SaveChanges() == 1 ? true : false;
        }
    }
}