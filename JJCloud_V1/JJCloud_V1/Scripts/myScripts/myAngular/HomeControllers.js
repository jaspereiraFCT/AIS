﻿var myApp = angular.module('myApp', ['ngGrid', 'nvd3ChartDirectives']);

myApp.controller('tablesController', ['$scope', '$rootScope', function ($scope, $rootScope) {

    // Reference the auto-generated proxy for the hub.
    $.connection.hub.url = "/signalr"
    var connectionStarted = false;

    var WsHub = $.connection.WsHub;
    WsHub.client.receiveGadgets = function (message) {
       //console.log(JSON.stringify(message));
        $scope.myData.push(message);
        $scope.$apply();
    };

    // Start the connection.
    $.connection.hub.start({ transport: 'longPolling' }).done(function () {
        //console.log("Connection OK!");
        connectionStarted = true;
        WsHub.server.getAllGadgets().done(function (consultas) {
            $scope.myData = consultas;
            $scope.$apply();
        });

    });

    $scope.myGadgetsSelected = [];

    $scope.gridOptions = {
        data: 'myData',
        selectedItems: $scope.myGadgetsSelected,
        afterSelectionChange: function (data) {
            if ($scope.myGadgetsSelected[0] !== undefined) {

                $scope.showHomeGrid = false;
                $scope.showLuzGrid = false;

                if ($scope.myGadgetsSelected[0].typeName.toLowerCase() == "luz") {
                    WsHub.server.getLigthStatus($scope.myGadgetsSelected[0].myID).done(function (consultas) {
                        $scope.light = consultas;
                        $scope.showLuzGrid = true;

                    });
                }

                if ($scope.myGadgetsSelected[0].typeName.toLowerCase() == "home") {
                    WsHub.server.getHomeTemp($scope.myGadgetsSelected[0].myID).done(function (consultas) {
                        $scope.home = consultas;
                        $scope.showHomeGrid = true;
                    });
                }

                WsHub.server.getAllSlaves($scope.myGadgetsSelected[0].myID).done(function (consultas) {
                    //console.log(JSON.stringify(consultas));
                    $rootScope.$broadcast('selected_Item', $scope.myGadgetsSelected[0].myID);
                    $scope.myGadgetData = consultas;
                });

                //$scope.$apply();
            }
            else {
                $rootScope.$broadcast('selected_Item', -1 );
            }
        },
        showFooter: false,
        showFilter: true,
        showHeader: true,
        showColumnMenu: false,
        showGroupPanel: false,
        showSelectionCheckbox: false,
        enablePaging: true,
        enableHighlighting: true,
        enableRowReordering: false,
        enableColumnResize: true,
        multiSelect: false,
        columnDefs: [{ field: 'myID', displayName: 'ID' },
                 { field: 'Name', displayName: 'Friendly Name' },
                 { field: 'typeName', displayName: 'Type' }]
    };

    $scope.gridOptions2 = {
        data: 'myGadgetData',
        showFooter: false,
        showFilter: true,
        showHeader: true,
        showColumnMenu: false,
        showGroupPanel: false,
        showSelectionCheckbox: false,
        enablePaging: true,
        enableHighlighting: true,
        enableRowReordering: false,
        enableColumnResize: true,
        multiSelect: false,
        enableRowSelection: false,
        columnDefs: [{ field: 'SlaveID', displayName: 'ID' },
                 { field: 'SlaveName', displayName: 'Friendly Name' },
                 { field: 'SlaveType', displayName: 'Type' }]

    };

}]);

myApp.controller('graphicController', ['$scope', function ($scope) {

    $.connection.hub.url = "/signalr"
    var connectionStarted = false;

    var WsHub = $.connection.WsHub;
    WsHub.client.receiveMasterPie = function (message) {
        //console.log(JSON.stringify(message));
        $scope.myData1 = message;
        $scope.$apply();
    };

    // Start the connection.
    $.connection.hub.start({ transport: 'longPolling' }).done(function () {
        //console.log("Connection OK!");
        connectionStarted = true;
        WsHub.server.getAllMasterPies().done(function (consultas) {
            $scope.myData1 = consultas;
            $scope.$apply();
        });

    });

    $scope.$on('selected_Item', function (response,dados) {
        if (connectionStarted === true) {
            if (dados === -1 || dados === undefined) {
                $scope.myData2 = [];
            }
            else {
                WsHub.server.getAllSlavePies(dados).done(function (consultas) {
                    $scope.myData2 = consultas;
                    $scope.$apply();
                });
            }
        }
    })

    $scope.xFunction = function () {
        return function (d) {
            return d.Name;
        };
    }

    $scope.yFunction = function () {
        return function (d) {
            return d.Count;
        };
    }

}]);