﻿using JJCloud_V1.Models;
using JJCloud_V1.Models.Dicionary;
using JJCloud_V1.Models.Repositorys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace JJCloud_V1.Controllers
{
    public class AISController : ApiController
    {
        #region DB Repositorys
        private readonly ConsultasRepository ConsultasRep = new ConsultasRepository();
        private readonly GadgetTypeRepository GadgetTypeRep = new GadgetTypeRepository();
        private readonly GadgetRepository GadgetRep = new GadgetRepository();
        private readonly HomeTempRepository HomeTempRep = new HomeTempRepository();
        private readonly LightStatusRepository LightRep = new LightStatusRepository();
        #endregion

        #region Temperature

        /// <summary>
        /// Define  intervalo  de  temperatura  que  o  utilizador  deseja
        /// </summary>
        /// <param name="id_House">Home ID</param>
        /// <param name="tMin">Temperatura Minima</param>
        /// <param name="tMax">Temperatura Máxima</param>
        /// <returns></returns>
        [ActionName(MyDic.Temperature_Set)]
        public Boolean GetTemperatureSet(long id_House, float tMin, float tMax)
        {
            bool res = false;
            Gadget _House = GadgetRep.Get(id_House);

            if (_House != null && GadgetTypeRep.Get(MyDic.HOME).Id == _House.GadgetTypeId)
            {
                HomeTemp _HouseTemp = HomeTempRep.Get(id_House);
                if (_HouseTemp == null)
                {
                    res = HomeTempRep.Add(new HomeTemp { maxTemp = decimal.Parse(tMax.ToString()), minTemp = decimal.Parse(tMin.ToString()) }) == null ? false : true;
                }
                else
                {
                    _HouseTemp.maxTemp = decimal.Parse(tMax.ToString());
                    _HouseTemp.minTemp = decimal.Parse(tMin.ToString());
                    res = HomeTempRep.Update(_HouseTemp);
                }
            }

            ConsultasRep.Add(new Consultas { WsName = MyDic.Temperature_Set, WsResult = res.ToString(), WsType = MyDic.TEMPERATURE, Date = DateTime.UtcNow });
            return res;

        }

        /// <summary>
        /// Subir ou descer a temperatura que está definida se value não for defenido então o seu valor será 0
        /// </summary>
        /// <param name="id_House">Home ID</param>
        /// <param name="direction"> String da direcção values = "up" || "down"</param>
        /// <param name="value">Número de graus a aumentar/diminiuir</param>
        /// <returns></returns>
        [ActionName(MyDic.Temperature_Alter)]
        public Boolean GetTemperatureAlter(long id_House, String direction, float value = 0)
        {
            bool res = false;
            Gadget _House = GadgetRep.Get(id_House);

            if (_House != null && GadgetTypeRep.Get(MyDic.HOME).Id == _House.GadgetTypeId)
            {

                HomeTemp _HouseTemp = HomeTempRep.Get(id_House);

                if (_HouseTemp.actualTemp != null)
                {
                    switch (direction.ToLower())
                    {
                        case "up": _HouseTemp.actualTemp += decimal.Parse(value.ToString()); break;
                        case "down": _HouseTemp.actualTemp -= decimal.Parse(value.ToString()); break;
                        default: return false;
                    }

                    res = HomeTempRep.Update(_HouseTemp);
                }
            }

            ConsultasRep.Add(new Consultas { WsName = MyDic.Temperature_Alter, WsResult = res.ToString(), WsType = MyDic.TEMPERATURE, Date = DateTime.UtcNow });
            return res;

        }

        /// <summary>
        /// Valor  da temperatura  atual
        /// </summary>
        /// <param name="id_House">Home ID</param>
        /// <returns></returns>
        [ActionName(MyDic.Temperature_Status)]
        public String GetTemperatureStatus(long id_House)
        {
            string res = "";
            Gadget _House = GadgetRep.Get(id_House);

            if (_House != null && GadgetTypeRep.Get(MyDic.HOME).Id == _House.GadgetTypeId)
            {

                HomeTemp _HouseTemp = HomeTempRep.Get(id_House);
                if (_HouseTemp.actualTemp != null)
                {
                    res = _HouseTemp.actualTemp.ToString();
                }
            }

            ConsultasRep.Add(new Consultas { WsName = MyDic.Temperature_Status, WsResult = res, WsType = MyDic.TEMPERATURE, Date = DateTime.UtcNow });
            return res;
        }


        /// <summary>
        /// Ar condicionado actualiza  a  temperatura  da  casa
        /// </summary>
        /// <param name="id_House">Home ID</param>
        /// <param name="temp">Temperatura da casa actual</param>
        /// <returns></returns>
        [ActionName(MyDic.Temperature_Update)]
        public Boolean GetTemperatureUpdate(long id_House, float temp)
        {
            bool res = false;
            Gadget _House = GadgetRep.Get(id_House);

            if (_House != null && GadgetTypeRep.Get(MyDic.HOME).Id == _House.GadgetTypeId)
            {

                HomeTemp _HouseTemp = HomeTempRep.Get(id_House);

                if (_HouseTemp == null)
                {
                    res = HomeTempRep.Add(new HomeTemp { actualTemp = decimal.Parse(temp.ToString()), Id = (int)id_House }) == null ? false : true;
                }
                else
                {
                    _HouseTemp.actualTemp = decimal.Parse(temp.ToString());
                    res = HomeTempRep.Update(_HouseTemp);
                }

            }

            ConsultasRep.Add(new Consultas { WsName = MyDic.Temperature_Update, WsResult = res.ToString(), WsType = MyDic.TEMPERATURE, Date = DateTime.UtcNow });
            return res;
        }

        #endregion

        #region Home

        /// <summary>
        /// Automóvel  pede  autorização para entrar na garagem
        /// </summary>
        /// <param name="id_carro">Carro ID</param>
        /// <returns></returns>
        [ActionName(MyDic.Access_Request)]
        public Boolean GetAccessRequest(long id_carro)
        {
            bool res = false;

            Gadget _carro = GadgetRep.Get(id_carro);

            if (_carro != null && _carro.GadgetId.HasValue && GadgetTypeRep.Get(MyDic.HOME).Id == _carro.GadgetId)
            {
                res = HomeTempRep.Get(GadgetRep.Get((long)_carro.GadgetId).Id).GarageClear;
            }

            ConsultasRep.Add(new Consultas { WsName = MyDic.Access_Request, WsResult = res.ToString(), WsType = MyDic.HOMEWS, Date = DateTime.UtcNow });
            return res;
        }

        /// <summary>
        /// Casa informa do status da garagem
        /// </summary>
        /// <param name="id_House">House ID</param>
        /// <returns></returns>
        [ActionName(MyDic.Access_Request_Put)]
        public Boolean GetAccessRequest_Put(long id_House)
        {
            bool res = false;

            HomeTemp _houseTemp = HomeTempRep.Get(id_House);

            if (_houseTemp != null)
            {
                _houseTemp.GarageClear = true;
                res = HomeTempRep.Update(_houseTemp);
            }

            ConsultasRep.Add(new Consultas { WsName = MyDic.Access_Request_Put, WsResult = res.ToString(), WsType = MyDic.HOMEWS, Date = DateTime.UtcNow });
            return res;
        }

        /// <summary>
        /// Lista de dispositivos que estão disponíveis  para uma casa if Type unknown return everting from house
        /// </summary>
        /// <param name="id_House">Home ID</param>
        /// <param name="Type">Tipo de dispositivos</param>
        /// <returns></returns>
        [ActionName(MyDic.Get_Device_List)]
        public IEnumerable<long> GetDeviceList(long id_House, string Type = "empty")
        {
            IEnumerable<long> res = new List<long>();

            GadgetType thisType = GadgetTypeRep.Get(Type);

            if (GadgetRep.Get(id_House) != null)
            {
                if (thisType == null)
                {
                    res = GadgetRep.GetAllFrom(id_House).Select(c => c.Id);
                }
                else
                {
                    res = GadgetRep.GetAllFrom(id_House, thisType.Id).Select(c => c.Id);
                }
            }

            ConsultasRep.Add(new Consultas { WsName = MyDic.Get_Device_List, WsResult = res.ToList().Count.ToString(), WsType = MyDic.HOMEWS, Date = DateTime.UtcNow });
            return res;
        }

        /// <summary>
        /// Ativar/desactivar o alarme de incendio
        /// </summary>
        /// <param name="id_House">Home ID</param>
        /// <returns></returns>
        [ActionName(MyDic.Fire_Alarm_Put)]
        public Boolean GetFireAlarm_Put(long id_House, bool state = true)
        {
            bool res = false;

            HomeTemp _houseTemp = HomeTempRep.Get(id_House);

            if (_houseTemp != null)
            {
                _houseTemp.FireAlarm = state;
                res = HomeTempRep.Update(_houseTemp);
            }

            ConsultasRep.Add(new Consultas { WsName = MyDic.Fire_Alarm_Put, WsResult = res.ToString(), WsType = MyDic.HOMEWS, Date = DateTime.UtcNow });
            return res;
        }

        /// <summary>
        /// Ver o estado do alarme de incendio
        /// </summary>
        /// <param name="id_House">Home ID</param>
        /// <returns></returns>
        [ActionName(MyDic.Fire_Alarm)]
        public Boolean GetFireAlarm(long id_House)
        {
            bool res = false;

            HomeTemp _homeTemp = HomeTempRep.Get(id_House);

            if (_homeTemp != null)
            {
                res = _homeTemp.FireAlarm;
            }

            ConsultasRep.Add(new Consultas { WsName = MyDic.Fire_Alarm, WsResult = res.ToString(), WsType = MyDic.HOMEWS, Date = DateTime.UtcNow });
            return res;
        }

        /// <summary>
        /// Verifica o estado das luzes
        /// </summary>
        /// <param name="id_house">Home ID</param>
        /// <returns></returns>
        [ActionName(MyDic.Light_Update_Status)]
        public Boolean GetLightUpdateStatus(long id_house)
        {
            bool res = false;



            ConsultasRep.Add(new Consultas { WsName = MyDic.Light_Update_Status, WsResult = res.ToString(), WsType = MyDic.HOMEWS, Date = DateTime.UtcNow });
            return res;
        }

        /// <summary>
        /// Saber o  estado das luzes
        /// </summary>
        /// <param name="id_House">Home ID</param>
        /// <returns></returns>
        [ActionName(MyDic.Light_Status)]
        public Boolean GetLightStatus(long id_House)
        {
            bool res = false;

            LightStatus _ls = LightRep.Get(id_House);

            if (_ls != null)
            {
                res = _ls.Status;
            }

            ConsultasRep.Add(new Consultas { WsName = MyDic.Light_Status, WsResult = res.ToString(), WsType = MyDic.HOMEWS, Date = DateTime.UtcNow });
            return res;
        }

        /// <summary>
        /// Atualização  do estado das luzes
        /// </summary>
        /// <param name="id_house">Home ID</param>
        /// <param name="status">string status values= "on" || "off"</param>
        /// <returns></returns>
        [ActionName(MyDic.Light_Update)]
        public Boolean GetLightUpdate(long id_house, string status)
        {
            bool res = false;

            LightStatus _ls = LightRep.Get(id_house);

            if (_ls != null)
            {
                _ls.Status = status.ToLower() == "on" ? true : false;
                res = LightRep.Update(_ls);
            }

            ConsultasRep.Add(new Consultas { WsName = MyDic.Light_Update, WsResult = res.ToString(), WsType = MyDic.HOMEWS, Date = DateTime.UtcNow });
            return res;
        }

        /// <summary>
        ///  Frigorifico fazer compras dos produtos  em  falta
        /// </summary>
        /// <param name="id_house">Home ID</param>
        /// <param name="jsonString">Json String with products list</param>
        /// <returns></returns>
        [ActionName(MyDic.Purchase_Order)]
        public Boolean GetPurchaseOrder(long id_house, string jsonString)
        {
            bool res = false;

            ConsultasRep.Add(new Consultas { WsName = MyDic.Purchase_Order, WsResult = res.ToString(), WsType = MyDic.HOMEWS, Date = DateTime.UtcNow });
            return res;
        }

        #endregion

        #region Plataform

        /// <summary>
        /// Registar  um utilizador  (casa,  automóvel,  AC,  etc.)
        /// </summary>
        /// <param name="name">User Name</param>
        /// <param name="Type">Tipo de dispositivo a ser associado</param>
        /// <returns>If added return ID else return -1</returns>
        [ActionName(MyDic.Register_User)]
        public long GetRegisterUser(string name, string Type)
        {
            long res = -1;
            if (Type.ToUpper() == MyDic.HOME || Type.ToUpper() == MyDic.CARRO || Type.ToUpper() == MyDic.LUZ || Type.ToUpper() == MyDic.AC)
            {


                GadgetType gT = GadgetTypeRep.Get(Type);

                if (gT == null)
                {
                    gT = GadgetTypeRep.Add(new GadgetType { typeName = Type });
                }

                res = GadgetRep.add(name, gT).Id;

                if (res != -1)
                {
                    switch (Type.ToUpper())
                    {
                        case MyDic.HOME: HomeTempRep.Add(new HomeTemp { GarageClear = true, FireAlarm = false, Id = res }); break;
                        case MyDic.LUZ: LightRep.Add(new LightStatus { Id = res }); break;
                        default: break;
                    }

                }

            }

            ConsultasRep.Add(new Consultas { WsName = MyDic.Register_User, WsResult = res.ToString(), WsType = MyDic.Plataform, Date = DateTime.UtcNow });
            return res;
        }

        /// <summary>
        /// Associar um utilizadoWr a outro, como exemplo é associar um  automóvel  a uma casa, para assim possibilitar o acesso à mesma
        /// </summary>
        /// <param name="id_master">Master ID</param>
        /// <param name="id_slave">Slave ID</param>
        /// <returns></returns>
        [ActionName(MyDic.Entity_Association)]
        public Boolean GetEntityAssociation(long id_master, long id_slave)
        {
            bool res = false;

            Gadget master = GadgetRep.Get(id_master);
            Gadget slave = GadgetRep.Get(id_slave);

            if (master != null && slave != null)
            {
                slave.GadgetId = master.Id;
                res = GadgetRep.Update(slave);
            }

            ConsultasRep.Add(new Consultas { WsName = MyDic.Entity_Association, WsResult = res.ToString(), WsType = MyDic.Plataform, Date = DateTime.UtcNow });
            return res;
        }

        #endregion
    }
}
